import json
import math
from dataclasses import dataclass
from typing import List, Dict, Tuple
from copy import deepcopy
import random
import signal
from contextlib import contextmanager


Point = List[int]


@dataclass
class Enemy:
    name: str
    health: int
    armor_bonus: int
    location: Point
    weapon: str


@dataclass
class Unit:
    name: str                # The name of the unit. Use this name to perform actions with this unit.
    type_: str               # The type of the unit. The type defines the base values.
    armor: str               # The name of the armour the unit is wearing.
    movement: int            # The maximum distance the unit can travel when using both actions.
    vision: int              # The maximum distance the unit can spot enemies and see changes in the brawl.
    aim: int                 # The aim of the unit, which is added to the hit chance of a weapon.
    health: int              # The current health of the unit. This can be restored or boosted using a medikit.
    max_health: int          # The maximum health the unit can have. Any health above the maximum health is reduced by 1 at the start of the unit's turn.
    armor_bonus: int         # The amount of damage that is negated before it reduces health. armorBonus is gained from a shield, but can be lost if taken damage from a grenade.
    max_armor_bonus: int     # The original amount of armorBonus the unit had at the start of the brawl.
    weapon: str              # The name of the weapon the unit is carrying.
    weapon_mags: int         # The number of magazines the unit is carrying for the weapon. One magazine is used immediately (if available) to reload the weapon at the start of the brawl.
    ammo: int                # The amount of ammunition currently loaded into the weapon. The weapon can be used as long as it has ammunition. If the ammunition reaches zero, it must be reloaded to shoot or to start overwatching.
    max_ammo: int            # The maximum amount of ammunition that can be loaded to the carried weapon. When a weapon is reloaded then the amount of ammunition that is currently loaded becomes this value.
    items: List[str]         # A list of the item names that the unit is carrying.
    location: Point          # The location in [x,y] coordinates where the unit is currently located.



@dataclass
class State:
    height_map: List[List[int]]
    map_width: int
    map_height: int
    units: Dict[str, Unit]
    enemies: Dict[str, Enemy]
    my_turn: bool
    waypoints: List[Point]


def parse_unit(unit: dict) -> Unit:
    """ json -> dataclass """
    return Unit(
        name = unit["name"],
        type_ = unit["type"],
        armor = unit["armor"],
        movement = unit["movement"],
        vision = unit["vision"],
        aim = unit["aim"],
        health = unit["health"],
        max_health = unit["maxHealth"],
        armor_bonus = unit["armorBonus"],
        max_armor_bonus = unit["maxArmorBonus"],
        weapon = unit["weapon"],
        weapon_mags = unit["weaponMags"],
        ammo = unit["ammo"],
        max_ammo = unit["maxAmmo"],
        items = unit["items"],
        location = unit["location"]
    )


def parse_notification(state: State, msg: dict) -> State:
    action = msg["action"]
    new_state = deepcopy(state)

    # I can't wait for PEP622...
    if action == "setup":
        """ Contains both the heightMap and the units. Very likely the first message.
            If the opponent started first and did something that caused a
            notification on their first turn, then it may appear before this.
            This always comes before the first 'startTurn' message.
        """
        new_state.height_map = msg["heightMap"]
        new_state.map_width = len(new_state.height_map)
        new_state.map_height = len(new_state.height_map[0])
        new_state.units = {u["name"]: parse_unit(u) for u in msg["units"]}
        new_state.waypoints = sorted([
                [0, 0],
                [0, new_state.map_height],
                [new_state.map_width, new_state.map_height],
                [new_state.map_width, 0],
            ],
            key = lambda p: dist(p, list(new_state.units.values())[0].location),
            reverse=True
        )
        print(f"Read {len(new_state.units)} units.")

    elif action == "enemy":
        """ Information on an enemy unit. Contains a reason attribute,
            which describes the reason for this message.  It could be for
            spotting a new enemy, or the enemy moved, or it became invisible
            or visible.
        """
        enemy = Enemy(
            name=msg["name"],
            health=int(msg["health"]),
            armor_bonus=int(msg["armorBonus"]),
            location=msg["location"],
            weapon=msg["weapon"]
        )
        new_state.enemies[enemy.name] = enemy
        print(f"Spotted new enemy: {enemy}")

    elif action == "startTurn":
        new_state.my_turn = True

    elif action == "confirm":
        """ Confirms the success of a message or instruction that the bot had sent.
            Side effects depend on the instruction and the action that was requested.
        """
        print(f"Action confirmed: {msg}")

    elif action == "deny":
        """ Denies a message or instruction that the bot sent to the host.
            The instruction was disregarded and the action was unsuccessful.
            This should not have any side effects.
        """
        print(f"Action denied: {msg}")

    elif action == "move":
        """ Friendly unit has moved.  Contains the name of the unit, its path,
            total distance moved, the final location and the amount of action
            points the unit has left
        """
        new_unit = deepcopy(state.units[msg["unit"]])
        new_location = msg["location"]
        new_unit.location = new_location
        new_state.units[new_unit.name] = new_unit
        print(f"{new_unit.name} moved to {new_unit.location}")

        # remove waypoint if close enough
        WAYPOINT_THRESHOLD = 10
        if state.waypoints and dist(new_unit.location, state.waypoints[0]) < WAYPOINT_THRESHOLD:
            print(f"Unit {new_unit.name} made it to waypoint {new_state.waypoints}")
            new_state.waypoints = new_state.waypoints[1:]

    elif action == "shot":
        """ A unit shot with their weapon. Contains damage, hit, crit chance.
            It also has source location, target location and the exact path of
            the shot.
        """
        attacker = msg["unit"]
        target = msg["target"]

        if msg["hit"] and target in state.units:
            # one of my units hit
            new_unit = deepcopy(state.units[target])
            new_unit.health -= msg["damage"]
            if new_unit.health > 0:
                new_state.units[target] = new_unit
            else:
                del new_state.units[target]

        if msg["hit"] and target in state.enemies:
            # enemy hit!
            new_enemy = deepcopy(state.enemies[target])
            new_enemy.health -= msg["damage"]
            if new_enemy.health > 0:
                new_state.enemies[target] = new_enemy
            else:
                del new_state.enemies[target]

        print(f"{attacker} shoots at {target}")

    elif action == "item":
        """ Enemy used an item. This is either a medikit or a grenade.
            Contains the unit and the target location.
        """
        pass
    elif action == "damage":
        """ A unit has received damage from an item. This is either from a
            medikit or a grenade. Contains the unit damaging and the target
            being damaged, as well as the damage amount and the health after
            the damage.
        """
        pass
    elif action == "obstacleShot":
        """ An obstacle has been shot at. Describes the shot and whether the
            obstacle was hit with the shot.
        """
        pass
    elif action == "obstacleUpdate":
        """ An obstacle on the heightMap has been updated. Describes the
            locations of the obstacles and the new height value for those obstacles.
        """
        pass

    return new_state


def take_turn(state: State) -> State:
    """ Take a turn, moving or attacking with all units. """
    state = deepcopy(state)
    if state.enemies:
        # find enemy with lowest health
        weakest_enemy = min(
            state.enemies.values(),
            key = lambda e: e.health
        )
        print(f"Attacking enemy: {weakest_enemy}")
        # attack with all characters
        for unit_name, unit in state.units.items():
            new_unit = attack(unit, weakest_enemy.location)
            state.units[unit_name] = new_unit
    else:
        # patrol the map
        # 1. select destination
        if state.waypoints:
            # move to first waypoint if exists
            dest = state.waypoints[0]
        else:
            # move to center if no waypoints
            dest = [state.map_width // 2, state.map_height // 2]

        # 2. move to destination
        for unit in state.units.values():
            move_towards(state, unit, dest)

    state = end_turn(state)
    return state


def dist(a: List[int], b: List[int]) -> float:
    """ Euclidean distance between two 2d points """
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)


def move_towards(state: State, unit: Unit, target: List[int]):
    """ Move towards 2d point `target`.
        Restricted by distance `unit.movement` and bounds of map.
    """
    # go as far in direction as `unit.movement` reaches
    # do this by finding scalar between target vector and
    # movement destination vector
    target_vec = [float(target[0] - unit.location[0]), float(target[1] - unit.location[1])]
    target_distance = dist(target, unit.location)
    if target_distance > unit.movement:
        factor = (unit.movement / target_distance)
        factor *= 0.7 + (random.random() * 0.3)  # introduce some randomness ;)

        # scale move vector by factor
        dest_vec = [target_vec[0] * factor,
                    target_vec[1] * factor]
    else:
        dest_vec = target_vec

    # find destination
    dest = [unit.location[0] + dest_vec[0], unit.location[1] + dest_vec[1]]

    # "truncate" move to bounds
    dest[0] = int(max([0, dest[0]]))
    dest[0] = int(min([state.map_width, dest[0]]))
    dest[1] = int(max([0, dest[1]]))
    dest[1] = int(min([state.map_height, dest[1]]))

    print(f"DEBUG: tried {unit.location} -> {target}")
    print(f"DEBUG: did   {unit.location} -> {dest}")
    # send move
    print(json.dumps({
        "action": "move",
        "unit": unit.name,
        "target": dest
    }))


def attack(unit: Unit, target: List[int]) -> Unit:
    """ Attack with `unit`. Reload if no ammo."""
    new_unit = deepcopy(unit)
    if unit.ammo > 0:
        print(json.dumps({
            "action": "attack",
            "unit": unit.name,
            "item": unit.weapon,
            "target": target,
            "ignoreAllies": False
        }))
        new_unit.ammo = new_unit.ammo - 1
    elif unit.weapon_mags > 0:
        # reload!
        print(json.dumps({
            "action": "reload",
            "unit": unit.name
        }))
        new_unit.ammo = new_unit.max_ammo
        new_unit.weapon_mags = new_unit.weapon_mags - 1
    else:
        print(f"{unit.name} is out of ammo!")
    return new_unit


def end_turn(state: State) -> State:
    """ Send message to end turn,
        and mutate state to indicate turn
    """
    new_state = deepcopy(state)
    print(json.dumps({"action": "endTurn"}))
    new_state.my_turn = False
    return new_state


# init empty state
state = State(
    height_map = [],
    map_width = 0,
    map_height = 0,
    units = {},
    enemies = {},
    my_turn = False,
    waypoints = []
)

while True:
    # read an incoming message
    msg = json.loads(input())
    # update state with the new info
    state = parse_notification(state, msg)

    if state.my_turn:
        # execute a turn
        state = take_turn(state)
